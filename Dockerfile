FROM ubuntu:16.04

FROM python:3.7

COPY ./app /app
COPY ./kaggle_data /kaggle_data
COPY ./models /models
COPY ./requirements.txt /requirements.txt

RUN apt-get update
RUN apt install -y libgl1-mesa-glx

RUN pip install -r requirements.txt

ENTRYPOINT ["python3", "/app/inference_flask_with_html.py"]

