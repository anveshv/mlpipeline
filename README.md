Step 1:
Navigate to the directory of the docker file and execute the following commands

    docker build -t stageClassifier:latest .

    docker run -p 7080:7080 stageClassifier:latest

Step 2:
After successfully running the docker container, open the browser and hit the URL http://localhost:7080/, then you will be taken to the screen, Where we could upload the fundus scan, we could see the uploaded image and name of the file.


More details are avilable in design document.
