from flask import Flask, render_template, request
import cv2
from tensorflow.keras.layers import ( Dropout,Dense, Input, GlobalAveragePooling2D)
from tensorflow.keras.applications.densenet import DenseNet121
from tensorflow.keras.models import Model
import numpy as np
import os 

NUM_CLASSES = 5
SIZE = 300

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = './kaggle_data/'

@app.route('/')
def upload_f():
   return render_template('upload.html')


def create_model(input_shape, n_out):
    input_tensor = Input(shape=input_shape)
    base_model = DenseNet121(include_top=False,
                   weights=None,
                   input_tensor=input_tensor)
    ### Loading weights of pretrained Densenet model
    base_model.load_weights("./models/DenseNet-BC-121-32-no-top.h5")
    x = GlobalAveragePooling2D()(base_model.output)
    x = Dropout(0.5)(x)
    x = Dense(1024, activation='relu')(x)
    x = Dropout(0.5)(x)
    final_output = Dense(n_out, activation='softmax', name='final_output')(x)
    model = Model(input_tensor, final_output)
    set_trainable = False
    for layer in model.layers:
        if layer.name.find("conv5") != -1:
            set_trainable = True
        if set_trainable:
            layer.trainable = True
        else:
            layer.trainable = False
    return model

## Creating model object
model = create_model(
    input_shape=(SIZE,SIZE,3), 
    n_out=NUM_CLASSES)

## Loading weights of trained model
model.load_weights('./models/trained_model_epoch8_.h5')
print('Completed loading the trained model')

stage_li = ['No DR','Mild DR','Moderate DR',
            'Severe DR','Proliferative DR']


@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
      f = request.files['file']
      f.save(os.path.join(app.config['UPLOAD_FOLDER'],f.filename))
      path = f.filename
      image = cv2.imread('./kaggle_data/'+path)
      image = cv2.resize(image, (SIZE, SIZE))
      X = np.array((image[np.newaxis])/255)
      score_predict=((model.predict(X).ravel()*model.predict(X[:, ::-1, :, :]).ravel()*model.predict(X[:, ::-1, ::-1, :]).ravel()*model.predict(X[:, :, ::-1, :]).ravel())**0.25).tolist()
      label_predict = np.argmax(score_predict)
      print(label_predict)
      return stage_li[label_predict]


# main driver function
if __name__ == '__main__':

	app.run(host='0.0.0.0', port=7080)
